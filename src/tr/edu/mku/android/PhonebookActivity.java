package tr.edu.mku.android;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import tr.edu.mku.android.component.Links;
import tr.edu.mku.android.phonebook.PhonebookAdapter;
import tr.edu.mku.android.phonebook.PhonebookParser;
import tr.edu.mku.android.phonebook.PhonebookRecord;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;

public class PhonebookActivity extends ActionBarActivity {
	
	protected static List<PhonebookRecord> records = new ArrayList<PhonebookRecord>();
	protected static PhonebookAdapter adapter;
	protected ProgressDialog pDialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_phonebook);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
		
		String title = getIntent().getExtras().getString("title"); setTitle(title);
		if(records.size() < 1)
			new GetPhonebookRecordsTask(this).execute(Links.FEED_REHBER);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.phonebook, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		ListView recordsList;
		EditText txtSearch;
		
		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_phonebook,
					container, false);
			
			
			recordsList = (ListView) rootView.findViewById(R.id.recordsList);
			recordsList.setAdapter(adapter);
			
			txtSearch = (EditText) rootView.findViewById(R.id.txtSearch);
			txtSearch.addTextChangedListener(new TextWatcher() {
				
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					Log.d("PhonebookActivity", "Searching for : " + s);
					if(s.length() > 0)
						adapter.getFilter().filter(s);
				}
				
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count,
						int after) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub
					
				}
			});
			return rootView;
		}
	}

	private class GetPhonebookRecordsTask extends AsyncTask<String, Void, List<PhonebookRecord>> {
        private Activity context;
        private ProgressDialog pDialog;
        public GetPhonebookRecordsTask(Activity context) {
            this.context = context;
        	pDialog = new ProgressDialog(context);
        }

        @Override
        protected void onPreExecute() {
        	// TODO Auto-generated method stub
        	super.onPreExecute();
        	pDialog.setTitle("L�tfen bekleyiniz");
		    pDialog.setMessage("Rehber indiriliyor...");
		    pDialog.setCanceledOnTouchOutside(false);
		    pDialog.show();
        }
        

        protected void onPostExecute(List<PhonebookRecord> records) {
        	if(pDialog.isShowing()) pDialog.dismiss();
    		Log.d("PhonebookActivity", "Post Exec");

        	if(!records.isEmpty()) {
        		adapter = new PhonebookAdapter(context, new ArrayList<PhonebookRecord>(records));
        	} else {
        		Log.d("PhonebookActivity", "Couldn't get any results.");
        		adapter = new PhonebookAdapter(context, new ArrayList<PhonebookRecord>());
        	}
        	
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					getSupportFragmentManager().beginTransaction()
					.replace(R.id.container, new PlaceholderFragment())
					.commit();
				}
			});
        }

        /*
         * uses HttpURLConnection to make Http request from Android to download
         * the XML file
         */
        private String getXmlFromUrl(String urlString) {
            StringBuffer output = new StringBuffer("");
            int ctr = 0;

            try {
                InputStream stream = null;
                URL url = new URL(urlString);
                URLConnection connection = url.openConnection();
                Log.d("PhonebookActivity", "Connection opened.");
                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestMethod("GET");
                httpConnection.connect();

                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    Log.d("PhonebookActivity", "HTTP Response OK.");
                    stream = httpConnection.getInputStream();

                    BufferedReader buffer = new BufferedReader(
                            new InputStreamReader(stream));
                    String s = "";
                    Log.d("PhonebookActivity", "Start reading.");
                    while ((s = buffer.readLine()) != null) {
                    	output.append(s);
                        Log.d("PhonebookActivity", s);
                    	ctr++;
                    }
                    	
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }
            Log.d("PhonebookActivity", "Got " + ctr + " lines.");
            return output.toString();
        }

        @Override
        protected List<PhonebookRecord> doInBackground(String... urls) {
            List<PhonebookRecord> records = new ArrayList<PhonebookRecord>();
            String xml = null;
            String url = urls[0];
            //xml = getXmlFromUrl(url);
            Log.d("PhonebookActivity", "XML : " + xml);
            
			try {
				Log.d("PhonebookActivity", "Trying to parse");
            	SAXParserFactory factory = SAXParserFactory.newInstance();
            	SAXParser saxParser;
            	PhonebookParser handler = new PhonebookParser();
				saxParser = factory.newSAXParser();
                XMLReader xr = saxParser.getXMLReader();
                // Load XML for parsing.
                AssetManager assetManager = getAssets();
                InputStream inputStream = null;
                try {
                    inputStream = assetManager.open("rehber.xml");
                } catch (IOException e) {
                    Log.e("tag", e.getMessage());
                }

                xml = readTextFile(inputStream);
                
                InputStream stream = new ByteArrayInputStream(xml.getBytes());
                
                xr.setContentHandler(handler);
                xr.parse(new InputSource(stream));
                Log.d("PhonebookActivity", "Handler starting");
				records = handler.getRecords();
				Log.d("PhonebookActivity", "Retrieved " + records.size() + " records.");
			} catch (ParserConfigurationException | SAXException e) {
				Log.e("PhonebookActivity", e.toString());
			} catch (IOException e) {
				Log.e("PhonebookActivity", e.toString());
			}       
            return records;
        }
        
        private String readTextFile(InputStream inputStream) {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

            byte buf[] = new byte[1024];
            int len;
            try {
                while ((len = inputStream.read(buf)) != -1) {
                    outputStream.write(buf, 0, len);
                }
                outputStream.close();
                inputStream.close();
            } catch (IOException e) {

            }
            return outputStream.toString();
        }
    }

}

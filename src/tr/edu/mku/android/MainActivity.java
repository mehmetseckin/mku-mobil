package tr.edu.mku.android;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import tr.edu.mku.android.component.Links;
import tr.edu.mku.android.component.User;
import tr.edu.mku.android.meal.Meal;
import tr.edu.mku.android.meal.MealParser;
import tr.edu.mku.android.menu.MkuIconMenuAdapter;
import tr.edu.mku.android.menu.MkuListMenuAdapter;
import tr.edu.mku.android.menu.MkuMenu;
import tr.edu.mku.android.menu.MkuMenuOnItemClickListener;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {

	public static User user;
	private static MkuMenu mkuMenu;
	private MediaPlayer player;
	
	private static final int RESULT_SETTINGS = 4534;
	public static int CHECKEXAM_INTERVAL = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);	
		setContentView(R.layout.activity_main);
		showMenuFragment();
		initCheckExamService();
        initPlayer();
	} 	

	private void showMenuFragment() {			
		int menuStyle = getPreferredMenuStyle();
		initUser();

		mkuMenu = new MkuMenu();		
		mkuMenu.setStyle(menuStyle);
		mkuMenu.initialize(user.getType());
		
		if(mkuMenu.getStyle() == MkuMenu.STYLE_ICONS) {
			getSupportFragmentManager().beginTransaction()
			.replace(R.id.container, new MkuIconMenuFragment())
			.commit();
		} else if(mkuMenu.getStyle() == MkuMenu.STYLE_LIST) {	
			getSupportFragmentManager().beginTransaction()
			.replace(R.id.container, new MkuListMenuFragment())
			.commit();
		}
	}
	
	private void initUser() {
		SharedPreferences sharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(this);
		int userType = Integer.parseInt(sharedPrefs.getString("prefUserType", "" + User.TYPE_OGRENCI));
		
		user = new User(userType);
		user.setUsername(sharedPrefs.getString("prefUsername", ""));
		user.setPassword(sharedPrefs.getString("prefPassword", ""));
		
	}
	
	private void initCheckExamService() {
		SharedPreferences sharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(this);
		int interval = Integer.parseInt(sharedPrefs.getString("prefRefreshRate", "" + 0));
		CHECKEXAM_INTERVAL = interval;
		if(isCheckExamServiceRunning()) stopService(new Intent(this, CheckExamService.class));
		startService(new Intent(this, CheckExamService.class));
	}
	
	private boolean isCheckExamServiceRunning() {
	    ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
	    for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
	        if (CheckExamService.class.getName().equals(service.service.getClassName())) {
	            return true;
	        }
	    }
	    return false;
	}
	
	private int getPreferredMenuStyle() {
		SharedPreferences sharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(this);
		int menuStyle = Integer.parseInt(sharedPrefs.getString("prefMenuStyle", "" + MkuMenu.STYLE_ICONS));
		return menuStyle;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(android.view.MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			Intent intent = new Intent(this, SettingsActivity.class);
	        startActivityForResult(intent, RESULT_SETTINGS);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
        case RESULT_SETTINGS:
        	showMenuFragment();
    		initCheckExamService();
			Log.d("Main", "Result [SettingsActivity]");
        	break;
        }
	}

	public static class MkuListMenuFragment extends Fragment {

		ListView menu;
		MkuListMenuAdapter listMenuAdapter;
		
		public MkuListMenuFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_list_menu, container,
					false);
			
			menu = (ListView) rootView.findViewById(R.id.list_menu);
			listMenuAdapter = new MkuListMenuAdapter(getActivity(), mkuMenu.getItems());			
			menu.setAdapter(listMenuAdapter);
			menu.setOnItemClickListener(new MkuMenuOnItemClickListener(getActivity()));
			return rootView;
		}
	}
	
	public static class MkuIconMenuFragment extends Fragment {

		GridView menu;
		MkuIconMenuAdapter iconMenuAdapter;
		
		public MkuIconMenuFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_icon_menu, container,
					false);
			
			menu = (GridView) rootView.findViewById(R.id.menu_grid);
			iconMenuAdapter = new MkuIconMenuAdapter(getActivity(), mkuMenu.getItems());
			menu.setAdapter(iconMenuAdapter);			
			menu.setOnItemClickListener(new MkuMenuOnItemClickListener(getActivity()));
			return rootView;
		}
	}
	
	public void haberler() {
		Intent news = new Intent(MainActivity.this, RSSListActivity.class);
		news.putExtra("title", "Haberler");
		news.putExtra("feedUrl", Links.FEED_NEWS);
		Log.d("Main", "Launching RSSListActivity for News");
		startActivity(news);
	}
	
	public void duyurular() {
		Intent ann = new Intent(MainActivity.this, RSSListActivity.class);
		ann.putExtra("title", "Duyurular");
		ann.putExtra("feedUrl", Links.FEED_ANNOUNCEMENTS);
		Log.d("Main", "Launching RSSListActivity for Announcements!");
		startActivity(ann);
	}
	
	public void etkinlikler() {
		Intent events = new Intent(MainActivity.this, RSSListActivity.class);
		events.putExtra("title", "Etkinlikler");
		events.putExtra("feedUrl", Links.FEED_EVENTS);
		Log.d("Main", "Launching RSSListActivity for Events");
		startActivity(events);
	}
	
	public void akademikTakvim() {
		Intent cal = new Intent(MainActivity.this, WebActivity.class);
		cal.putExtra("title", "Akademik Takvim");
		cal.putExtra("url", Links.URL_AKADEMIK_TAKVIM);
		cal.putExtra("postData", "");
		Log.d("Main", "Launching WebActivity for Akademik Takvim");
		startActivity(cal);
	}
	
	public void ogrenciBilgiSistemi() {
		if(user.getUsername().equals("") || user.getPassword().equals("")) {
			Toast.makeText(this, "L�tfen Ayarlar'a gidip ��renci numaran�z� ve �ifrenizi kontrol ediniz.", Toast.LENGTH_LONG).show();
			return;
		}
		Intent obs = new Intent(MainActivity.this, WebActivity.class);
		obs.putExtra("title", "��renci Bilgi Sistemi");
		obs.putExtra("url", Links.SERVICE_OBS);
		obs.putExtra("postData", "okulno="+user.getUsername()+"&sifre="+user.getPassword()+"&kontrol=1");
		Log.d("Main", "Launching WebActivity for OBS");
		startActivity(obs);
	}
	
	public void ekDersBilgiSistemi() {
		if(user.getUsername().equals("") || user.getPassword().equals("")) {
			Toast.makeText(this, "L�tfen Ayarlar'a gidip e-mail adresinizi ve �ifrenizi kontrol ediniz.", Toast.LENGTH_LONG).show();
			return;
		}
		Intent edbs = new Intent(MainActivity.this, WebActivity.class);
		edbs.putExtra("title", "Ek Ders Bilgi Sistemi");
		edbs.putExtra("url", Links.SERVICE_EDBS);
		edbs.putExtra("postData", "mail="+user.getUsername()+"&sifre="+user.getPassword());
		Log.d("Main", "Launching WebActivity for EDBS");
		startActivity(edbs);
	}
	
	public void akademikBilgiSistemi() {
		if(user.getUsername().equals("") || user.getPassword().equals("")) {
			Toast.makeText(this, "L�tfen Ayarlar'a gidip e-mail adresinizi ve �ifrenizi kontrol ediniz.", Toast.LENGTH_LONG).show();
			return;
		}
		
		Intent abs = new Intent(MainActivity.this, WebActivity.class);
		abs.putExtra("title", "Akademik Bilgi Sistemi");
		abs.putExtra("url", Links.SERVICE_ABS);
		abs.putExtra("postData", "sicil="+user.getUsername()+"&pass="+user.getPassword()+"&submit=Tamam");
		Log.d("Main", "Launching WebActivity for ABS");
		startActivity(abs);	
		}
	
	public void kutuphaneKatalogTarama() {
		Intent kkt = new Intent(MainActivity.this, WebActivity.class);
		kkt.putExtra("title", "K�t�phane Katalog Tarama");
		kkt.putExtra("url", Links.URL_KATALOG_TARAMA);
		kkt.putExtra("postData", "");
		Log.d("Main", "Launching WebActivity for K�t�phane Katalog Tarama");
		startActivity(kkt);		
	}
	
	public void rehber() {
		Intent rehber = new Intent(MainActivity.this, PhonebookActivity.class);
		rehber.putExtra("title", "Rehber");
		rehber.putExtra("url", Links.FEED_REHBER);
		rehber.putExtra("postData", "");
		Log.d("Main", "Launching WebActivity for Rehber");
		startActivity(rehber);
	}
	
	public void radyoUniversite() {
		if(!player.isPlaying()) {
			AlertDialog aDialog = new AlertDialog.Builder(this)
			.setTitle("Radyo �niversite")
			.setMessage("Radyo �niversite yay�n�n� dinliyorsunuz. Dinlemeyi durdurmak i�in, Radyo �niversite logosuna tekrar dokunun.")
			.setPositiveButton("Tamam", new DialogInterface.OnClickListener() {
		
					public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
					}
				}).create();
			
			aDialog.show();
			
			startPlaying();
		}
		else {
			stopPlaying();
		}
	}
	
	public void gununYemegi() {
		new GetXMLTask(this).execute(Links.FEED_YEMEK_LISTESI);
	}
	
	private void initPlayer() {
		player = new MediaPlayer();
        try {
            player.setDataSource(Links.STREAM_RADYOUNIVERSITE);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
	}
    
	private void startPlaying() {
        player.prepareAsync();

        player.setOnPreparedListener(new OnPreparedListener() {

            public void onPrepared(MediaPlayer mp) {
                player.start();
            }
        });
    }

    private void stopPlaying() {
        if (player.isPlaying()) {
            player.stop();
            player.release();
            initPlayer();
        }
    }
    
    @Override
    protected void onPause() {
        super.onPause();
        if (player.isPlaying()) {
            player.stop();
        }
    }
    
    private class GetXMLTask extends AsyncTask<String, Void, List<Meal>> {
        private Activity context;
        private ProgressDialog pDialog;
        private AlertDialog aDialog;
        public GetXMLTask(Activity context) {
            this.context = context;
        	pDialog = new ProgressDialog(context);
        }

        @Override
        protected void onPreExecute() {
        	// TODO Auto-generated method stub
        	super.onPreExecute();
        	pDialog.setTitle("L�tfen bekleyiniz");
		    pDialog.setMessage("Yemek listesi kontrol ediliyor ...");
		    pDialog.setCanceledOnTouchOutside(false);
		    pDialog.show();
        }
        

        protected void onPostExecute(List<Meal> meals) {
        	if(pDialog.isShowing()) pDialog.dismiss();
    		Meal todaysMeal = null;
        	if(!meals.isEmpty()) {
        		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy", Locale.US);
        		String today = sdf.format(new Date());
        		for(Meal meal : meals) {
        			Log.d("MainActivity > Yemek listesi", meal.getDate() + " vs. " + today);
        			if(meal.getDate().equals(today)) {
        				todaysMeal = meal;
        			}
        		}
        	}
        	
        	if(todaysMeal != null) {
        		aDialog = new AlertDialog.Builder(context)
				.setTitle(todaysMeal.getDate())
				.setMessage(todaysMeal.getContent() + " (" + todaysMeal.getCals() + ")")
				.setPositiveButton("Tamam", new DialogInterface.OnClickListener() {
						
						public void onClick(DialogInterface dialog, int which) {
								dialog.dismiss();
						}
					}).create();
        		aDialog.show();
        	} else {
        		aDialog = new AlertDialog.Builder(context)
				.setTitle("Hata")
				.setMessage("Bug�n i�in yemek kayd� bulunamad�.")
				.setPositiveButton("Tamam", new DialogInterface.OnClickListener() {
						
						public void onClick(DialogInterface dialog, int which) {
								dialog.dismiss();
						}
					}).create();
        		aDialog.show();
        	}
        }

        /*
         * uses HttpURLConnection to make Http request from Android to download
         * the XML file
         */
        private String getXmlFromUrl(String urlString) {
            StringBuffer output = new StringBuffer("");
            try {
                InputStream stream = null;
                URL url = new URL(urlString);
                URLConnection connection = url.openConnection();

                HttpURLConnection httpConnection = (HttpURLConnection) connection;
                httpConnection.setRequestMethod("GET");
                httpConnection.connect();

                if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    stream = httpConnection.getInputStream();

                    BufferedReader buffer = new BufferedReader(
                            new InputStreamReader(stream));
                    String s = "";
                    while ((s = buffer.readLine()) != null)
                        output.append(s);
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return output.toString();
        }

        @Override
        protected List<Meal> doInBackground(String... urls) {
            List<Meal> meals = null;
            String xml = null;
            String url = urls[0];
            xml = getXmlFromUrl(url);
			try {
            	SAXParserFactory factory = SAXParserFactory.newInstance();
            	SAXParser saxParser;
            	MealParser handler = new MealParser();
				saxParser = factory.newSAXParser();
                XMLReader xr = saxParser.getXMLReader();
                InputStream stream = new ByteArrayInputStream(xml.getBytes());
                xr.setContentHandler(handler);
                xr.parse(new InputSource(stream));
				meals = handler.getMeals();
			} catch (ParserConfigurationException | SAXException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}       
            return meals;
        }
    }
}

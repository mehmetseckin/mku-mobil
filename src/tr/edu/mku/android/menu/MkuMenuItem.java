package tr.edu.mku.android.menu;

import android.R;

/**
 * Stores data for the custom menu items.
 * 
 * @author Mehmet Seckin
 *
 */
public class MkuMenuItem {
	private String caption;
	private int imageResource;
	private boolean enabled;
	public MkuMenuItem(String caption, int imageResource) {
		super();
		this.caption = caption;
		this.imageResource = imageResource;
		this.setEnabled(true);
	}
	
	public MkuMenuItem(String caption, int imageResource, boolean enabled) {
		super();
		this.caption = caption;
		this.imageResource = imageResource;
		this.setEnabled(enabled);
	}
	
	
	
	public MkuMenuItem() {
		this.caption = "Undefined";
		this.imageResource = R.drawable.ic_delete;
	}

	public String getCaption() {
		return caption;
	}
	public void setCaption(String caption) {
		this.caption = caption;
	}
	public int getImageResource() {
		return imageResource;
	}
	public void setImageResource(int imageResource) {
		this.imageResource = imageResource;
	}

	/**
	 * @return the enabled
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * @param enabled the enabled to set
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

}

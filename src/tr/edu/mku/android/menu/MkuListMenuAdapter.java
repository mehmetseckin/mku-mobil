package tr.edu.mku.android.menu;

import java.util.ArrayList;

import tr.edu.mku.android.R;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * Provides a List style menu for the application.
 * 
 * @author Mehmet Seckin
 *
 */
public class MkuListMenuAdapter extends BaseAdapter {
	private Activity activity;
	private ArrayList<MkuMenuItem> data;
	private static LayoutInflater inflater = null;

	public MkuListMenuAdapter(Activity a, ArrayList<MkuMenuItem> d) {
		super();
	    activity = a;
	    data=d;
	    inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View vi=convertView;
        if(convertView==null)
            vi = inflater.inflate(R.layout.item_menu, null);
 
        TextView caption = (TextView)vi.findViewById(R.id.txtCaption); // title
 
        MkuMenuItem menuItem = new MkuMenuItem();
        menuItem = data.get(position);
        Bitmap bm = BitmapFactory.decodeResource(activity.getResources(), menuItem.getImageResource());

        caption.setText(menuItem.getCaption());
        
        if(!menuItem.isEnabled()) {
        	caption.setTextColor(Color.GRAY);
            bm = adjustOpacity(bm, 128);
        }
        caption.setCompoundDrawablesWithIntrinsicBounds(new BitmapDrawable(bm), null, null, null);
        
        return vi;
	}

	//and here's where the magic happens
	private Bitmap adjustOpacity(Bitmap bitmap, int opacity)
	{
	    //make sure bitmap is mutable (copy of needed)
	    Bitmap mutableBitmap = bitmap.isMutable()
	                           ? bitmap
	                           : bitmap.copy(Bitmap.Config.ARGB_8888, true);

	    //draw the bitmap into a canvas
	    Canvas canvas = new Canvas(mutableBitmap);

	    //create a color with the specified opacity
	    int colour = (opacity & 0xFF) << 24;

	    //draw the colour over the bitmap using PorterDuff mode DST_IN
	    canvas.drawColor(colour, PorterDuff.Mode.DST_IN);

	    //now return the adjusted bitmap
	    return mutableBitmap;
	}

}

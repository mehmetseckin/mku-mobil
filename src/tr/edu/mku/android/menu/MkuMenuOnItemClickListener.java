package tr.edu.mku.android.menu;

import tr.edu.mku.android.MainActivity;
import android.app.Activity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

/**
 * Listens to the onItemClick events on the custom menu,
 * and calls necessary MainActivity methods.
 * 
 * @author Mehmet Seckin
 *
 */
public class MkuMenuOnItemClickListener implements OnItemClickListener {

	private Activity activity;
	
	public MkuMenuOnItemClickListener(Activity a) {
		activity = a;
	}
	
	@Override
	public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3) {
		MkuMenuItem item = (MkuMenuItem) adapter.getItemAtPosition(position);
		MainActivity mainActivity = (MainActivity) activity;
		if(item.isEnabled()) {
			if(item.getCaption().equals("Haberler")) {
				mainActivity.haberler();
				return;
			}
			if(item.getCaption().equals("Duyurular")) {
				mainActivity.duyurular();
				return;
			}
			if(item.getCaption().equals("Etkinlikler")) {
				mainActivity.etkinlikler();
				return;
			}
			if(item.getCaption().equals("Akademik Takvim")) {
				mainActivity.akademikTakvim();
				return;
			}		
			if(item.getCaption().equals("��renci Bilgi Sistemi")) {
				mainActivity.ogrenciBilgiSistemi();
				return;
			}
			if(item.getCaption().equals("Ek Ders Bilgileri")) {
				mainActivity.ekDersBilgiSistemi();
				return;
			}
			if(item.getCaption().equals("Akademik Bilgi Sistemi")) {
				mainActivity.akademikBilgiSistemi();
				return;
			}
			if(item.getCaption().equals("Telefon Rehberi")) {
				mainActivity.rehber();
				return;
			}
			if(item.getCaption().equals("K�t�phane Katalog Tarama")) {
				mainActivity.kutuphaneKatalogTarama();
				return;
			}
			if(item.getCaption().equals("G�n�n Yeme�i")) {
				mainActivity.gununYemegi();
				return;
			}
			if(item.getCaption().equals("Radyo �niversite 90.0")) {
				mainActivity.radyoUniversite();
			}
		}
		return;
	}

}

package tr.edu.mku.android.menu;

/**
 * Provides a custom Menu.
 */
import java.util.ArrayList;

import tr.edu.mku.android.R;
import tr.edu.mku.android.component.User;
import android.util.Log;

public class MkuMenu {
	public static final int STYLE_ICONS = 10;
	public static final int STYLE_LIST = 20;

	private int style;
	private ArrayList<MkuMenuItem> items;
	
	public void initialize(int userType) {
		items = new ArrayList<MkuMenuItem>();
		items.add(new MkuMenuItem("Haberler", R.drawable.ic_news));
		items.add(new MkuMenuItem("Duyurular", R.drawable.ic_announcements));
		items.add(new MkuMenuItem("Etkinlikler", R.drawable.ic_events));
		items.add(new MkuMenuItem("Akademik Takvim", R.drawable.ic_content_paste));
		//items.add(new MkuMenuItem("K�t�phane Katalog Tarama", R.drawable.kutuphane));
		items.add(new MkuMenuItem("G�n�n Yeme�i", R.drawable.ic_yemek));
		items.add(new MkuMenuItem("Telefon Rehberi", R.drawable.ic_rehber));
		if(userType == User.TYPE_OGRENCI) {
			Log.d("Main", "User: Ogrenci");
			items.add(new MkuMenuItem("��renci Bilgi Sistemi", R.drawable.ic_ogrenci));
			items.add(new MkuMenuItem("Akademik Bilgi Sistemi", R.drawable.ic_akademik, false));
			items.add(new MkuMenuItem("Ek Ders Bilgileri", R.drawable.ic_edbs, false));
		}
		else if(userType == User.TYPE_AKADEMISYEN) {
			Log.d("Main", "User: Akademisyen");
			items.add(new MkuMenuItem("��renci Bilgi Sistemi", R.drawable.ic_ogrenci, false));
			items.add(new MkuMenuItem("Akademik Bilgi Sistemi", R.drawable.ic_akademik));
			items.add(new MkuMenuItem("Ek Ders Bilgileri", R.drawable.ic_edbs));
		}
		items.add(new MkuMenuItem("Radyo �niversite 90.0", R.drawable.ic_radyo));
		
		this.setItems(items);
	}

	public int getStyle() {
		return style;
	}

	public void setStyle(int style) {
		this.style = style;
	}

	public ArrayList<MkuMenuItem> getItems() {
		return items;
	}

	public void setItems(ArrayList<MkuMenuItem> items) {
		this.items = items;
	}
}

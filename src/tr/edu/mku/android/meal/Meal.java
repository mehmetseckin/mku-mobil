package tr.edu.mku.android.meal;

/**
 * Stores the Meal data received from the XML service,
 * for the Meal of the Day feature.
 * 
 * @author Mehmet Seckin
 *
 */
public class Meal {

	private String date;
	private String content;
	private String cals;
	
	public Meal() {
		date = "01.01.1970";
		content = "Raw Rice and Water";
		cals = "4500 Calories";
	}
	
	public Meal(String date, String content, String cals) {
		this.date = date;
		this.content = content;
		this.cals = cals;
	}
	
	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}
	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}
	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}
	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}
	/**
	 * @return the cals
	 */
	public String getCals() {
		return cals;
	}
	/**
	 * @param cals the cals to set
	 */
	public void setCals(String cals) {
		this.cals = cals;
	}
}

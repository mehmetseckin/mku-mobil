package tr.edu.mku.android.meal;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * An XML parser class created specifically
 * for the meal web service.
 * 
 * @author Mehmet Seckin
 *
 */
public class MealParser extends DefaultHandler {

	private List<Meal> meals;
	private Meal meal;
	private String tempVal;

	public MealParser() {
		setMeals(new ArrayList<Meal>());
	}

	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		// Reset
		tempVal = "";
		if (qName.equalsIgnoreCase("yemek")) {
			// create a new instance of Meal
			meal = new Meal();
		}
	}

	public void characters(char[] ch, int start, int length)
			throws SAXException {
		tempVal = new String(ch, start, length);
	}

	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		if (qName.equalsIgnoreCase("yemekler")) {

		} else if (qName.equalsIgnoreCase("tarih")) {
			meal.setDate(tempVal);
		} else if (qName.equalsIgnoreCase("isim")) {
			meal.setContent(tempVal);
		} else if (qName.equalsIgnoreCase("kalori")) {
			meal.setCals(tempVal);
			getMeals().add(meal);
		}
	}

	/**
	 * @return the meals
	 */
	public List<Meal> getMeals() {
		return meals;
	}

	/**
	 * @param meals the meals to set
	 */
	public void setMeals(List<Meal> meals) {
		this.meals = meals;
	}
}

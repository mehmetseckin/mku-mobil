package tr.edu.mku.android;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import tr.edu.mku.android.component.Links;
import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

/**
 * Checks the exam results for a change, if there is a change,
 * creates a notification and informs the user.
 * 
 * @author Mehmet Seckin
 *
 */
public class CheckExamService extends Service {

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}
	
    private Handler mHandler = new Handler();
    private Timer mTimer = null;
	
    @Override
    public void onCreate() {
    	super.onCreate();
    	
    	if(mTimer != null) {
            mTimer.cancel();
        } else {
            mTimer = new Timer();
            if(MainActivity.CHECKEXAM_INTERVAL > 0) {
            	mTimer.scheduleAtFixedRate(new CheckExamResultsTimerTask(), 0, MainActivity.CHECKEXAM_INTERVAL);
            }
        }
    }
    
    /**
     * Checks the exam results periodically with a given time interval.
     * 
     * @author Mehmet Seckin
     *
     */
    class CheckExamResultsTimerTask extends TimerTask {
    	 
        @Override
        public void run() {
            mHandler.post(new Runnable() {
 
                @Override
                public void run() {
                	 /*HttpClient httpclient = new DefaultHttpClient();
                	 HttpPost httppost = new HttpPost(Links.SERVICE_OBS);
                	 // Add your data   
                	 List < NameValuePair > nameValuePairs = new ArrayList < NameValuePair > (5);
                	 nameValuePairs.add(new BasicNameValuePair("okulno", MainActivity.user.getUsername()));
                	 nameValuePairs.add(new BasicNameValuePair("sifre", MainActivity.user.getPassword()));
                	 nameValuePairs.add(new BasicNameValuePair("kontrol", "1"));
                	 try {
                	     httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                	     try {
                	         HttpResponse response = httpclient.execute(httppost);
                	     } catch (ClientProtocolException e) {
                	         e.printStackTrace();
                	     } catch (IOException e) {
                	         e.printStackTrace();
                	     }
                	 } catch (UnsupportedEncodingException e) {
                	     e.printStackTrace();
                	 } }*/
                 	// TODO: Check exam notes
                }
 
            });
        }
 
    }
}

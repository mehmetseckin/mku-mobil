package tr.edu.mku.android.rss;

import java.util.List;

import tr.edu.mku.android.R;


import android.app.Activity;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class RSSListAdapter extends ArrayAdapter<RSSArticle> {
	private int _layout_id, _thumb_id, _title_id;
	private Activity activity;
	public RSSListAdapter(Activity activity, List<RSSArticle> imageAndTexts, int layout_id, int thumb_id, int title_id) {
		super(activity, 0, imageAndTexts);
		this.activity = activity;
		_layout_id = layout_id;
		_thumb_id = thumb_id;
		_title_id = title_id;
	}


	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = activity.getLayoutInflater();

		// Inflate the views from XML
		View rowView = inflater.inflate(_layout_id, null);
		RSSArticle current = getItem(position);

		TextView textView = (TextView) rowView.findViewById(_title_id);
		ImageView imageView = (ImageView) rowView.findViewById(_thumb_id);

		if(current.getImg() != null ){
            imageView.setImageBitmap(current.getImg());
    	}
    	textView.setText(current.getTitle());


		return rowView;

	} 

}
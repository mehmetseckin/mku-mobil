package tr.edu.mku.android.rss;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;


import android.text.Html;
import android.util.Log;


public class RSSReader {
	
	private final static String BOLD_OPEN = "<B>";
	private final static String BOLD_CLOSE = "</B>";

	/**
	 * This method defines a feed URL and then calles our SAX Handler to read the article list
	 * from the stream
	 * 
	 * @return List<Article> - suitable for the List View activity
	 */
	public static List<RSSArticle> getLatestRssFeed(String feed){
		
		
		RSSHandler rh = new RSSHandler();
		List<RSSArticle> articles =  rh.getLatestArticles(feed);
		Log.i("RSS INFO", "Number of articles " + articles.size());
		return articles;
	}
	
	
	/**
	 * This method takes a list of Article objects and converts them in to the 
	 * correct JSON format so the info can be processed by our list view
	 * 
	 * @param articles - list<Article>
	 * @return List<JSONObject> - suitable for the List View activity
	 */
	protected static List<JSONObject> fillData(List<RSSArticle> articles) {

        List<JSONObject> items = new ArrayList<JSONObject>();
        for (RSSArticle article : articles) {
            JSONObject current = new JSONObject();
            try {
            	buildJsonObject(article, current);
			} catch (JSONException e) {
				Log.e("RSS ERROR", "Error creating JSON Object from RSS feed");
			}
			items.add(current);
        }
        
        return items;
	}


	/**
	 * This method takes a single Article Object and converts it in to a single JSON object
	 * including some additional HTML formating so they can be displayed nicely
	 * 
	 * @param article
	 * @param current
	 * @throws JSONException
	 */
	private static void buildJsonObject(RSSArticle article, JSONObject current) throws JSONException {
		String title = article.getTitle();
		String imgLink = article.getImgLink();
		String url = article.getUrl().toString();
		
		StringBuffer sb = new StringBuffer();
		sb.append(BOLD_OPEN).append(title).append(BOLD_CLOSE);
		
		current.put("text", Html.fromHtml(sb.toString()));
		current.put("imageLink", imgLink);
		current.put("url", url);
	}
}


package tr.edu.mku.android.rss;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;


import android.util.Log;

public class RSSHandler extends DefaultHandler {

	// TODO: Integrate this field to the general settings activity.
	private static final int MAX_ITEMS = 25;
	// Gecici depolama islemleri icin kullanacagimiz makale ve liste degiskenleri
	private RSSArticle currentArticle = new RSSArticle();
	private List<RSSArticle> articleList = new ArrayList<RSSArticle>();
	// Makale sayaci
	private int articlesAdded = 0;

	// Okunan karakterler icin string buffer.
	StringBuffer chars = new StringBuffer();

	
	/* 
	 * Bu method, her XML tag acilisi bulundugunda cagriliyor.
	 * Burada, StringBuffer'imizi reset ederek yeni deger icin hazirlik yapiyoruz.
	 *
	 * (non-Javadoc)
	 * @see org.xml.sax.helpers.DefaultHandler#startElement(java.lang.String, java.lang.String, java.lang.String, org.xml.sax.Attributes)
	 */
	public void startElement(String uri, String localName, String qName, Attributes atts) {
		chars = new StringBuffer();
	}



	/* 
	 * Bu method, her XML tag kapanisi bulundugunda cagriliyor.
	 * Burada, kapanan elemana gore veriyi alip makalemize yaziyoruz, ya da olusturulan makaleyi listeye
	 * kaydedip herseyi resteliyoruz.
	 * 
	 * (non-Javadoc)
	 * @see org.xml.sax.helpers.DefaultHandler#endElement(java.lang.String, java.lang.String, java.lang.String)
	 */
	public void endElement(String uri, String localName, String qName) throws SAXException {
		if (localName.equalsIgnoreCase("title"))
		{
			Log.d("RSSHandler", "Setting article title: " + chars.toString());
			currentArticle.setTitle(chars.toString());

		}
		else if (localName.equalsIgnoreCase("description"))
		{
			Log.d("RSSHandler", "Setting article description: " + chars.toString().substring(0, chars.toString().length()/5) + "...");
			currentArticle.setDescription(chars.toString());
		}
		else if (localName.equalsIgnoreCase("link"))
		{
			try {
				Log.d("RSSHandler", "Setting article link url: " + chars.toString());
				currentArticle.setUrl(new URL(chars.toString()));
			} catch (MalformedURLException e) {
				Log.e("RSSHandler", e.getMessage());
			}

		}
//		else if (localName.equalsIgnoreCase("pubDate")) {
//			Log.d("RSSHandler", "PubDate found. updating description as '" + "Yayinlanma tarihi : " + chars.toString() + "'...");
//			currentArticle.setDescription("Yayinlanma tarihi : " + chars.toString());
//		}
		// Makale bilgisi bitmis mi kontrol et
		if (localName.equalsIgnoreCase("item")) {

			articleList.add(currentArticle);
			
			currentArticle = new RSSArticle();

			// Limit degere ulasildi mi kontrol et
			articlesAdded++;
			if (articlesAdded >= MAX_ITEMS)
			{
				throw new SAXException("Reached max. article limit!");
			}
		}
	}
	
	



	/* 
	 * Bu method, RSS Feed'i okurken tag'ler arasinda karakterlere rastlandiginda cagriliyor.
	 * Burada sadece buffer'a eklemeye devam ediyoruz, endElement metodunda bunlarla ilgilenecegiz.
	 * 
	 * (non-Javadoc)
	 * @see org.xml.sax.helpers.DefaultHandler#characters(char[], int, int)
	 */
	public void characters(char ch[], int start, int length) {
		chars.append(new String(ch, start, length));
	}





	/**
	 * Bu metod, parser nesnemizin giris noktasi olacak. SAXParser ve XMLReader gibi
	 * yardimci araclari hazirlayip, parse edilmis nesne listesini geri dondurecek.
	 * 
	 * @param feedUrl
	 * @return
	 */
	public List<RSSArticle> getLatestArticles(String feedUrl) {
		URL url = null;
		try {

			SAXParserFactory spf = SAXParserFactory.newInstance();
			SAXParser sp = spf.newSAXParser();
			XMLReader xr = sp.getXMLReader();

			url = new URL(feedUrl);
			
			xr.setContentHandler(this);
			xr.parse(new InputSource(url.openStream()));


		} catch (IOException e) {
			Log.e("RSS Handler IO", e.getMessage() + " >> " + e.toString());
		} catch (SAXException e) {
			Log.e("RSS Handler SAX", e.toString());
		} catch (ParserConfigurationException e) {
			Log.e("RSS Handler Parser Config", e.toString());
		}
		
		return articleList;
	}

}
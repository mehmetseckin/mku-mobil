package tr.edu.mku.android;

import java.util.List;

import tr.edu.mku.android.rss.RSSArticle;
import tr.edu.mku.android.rss.RSSListAdapter;
import tr.edu.mku.android.rss.RSSReader;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class RSSListActivity extends ActionBarActivity {
	private String title;
	private String feedUrl;
	protected static List<RSSArticle> articles;
	protected static RSSListAdapter rssListAdapter;

	protected ProgressDialog pDialog, downloadDialog;
	protected AlertDialog noNetworkDialog;	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_rsslist);
		pDialog = new ProgressDialog(this);
		noNetworkDialog = new AlertDialog.Builder(this)
				.setTitle("Hata")
				.setMessage("Internet baglantisini saglayamadik. L�tfen baglantinizi kontrol edin ve tekrar deneyin.")
				.setPositiveButton("Tamam", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
			}
		}).create();
		new AsyncLoadArticles().execute();
		title 	= getIntent().getExtras().getString("title");
		feedUrl = getIntent().getExtras().getString("feedUrl");
		setTitle(title);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return super.onOptionsItemSelected(item);
	}

	public static class RSSListFragment extends Fragment {

		private ListView list;

		public RSSListFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_rss_list,
					container, false);
			
			list = (ListView) rootView.findViewById(R.id.list_rss);
			list.setAdapter(rssListAdapter);
			list.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> adapter, View v, 
						int position, long arg3) {
					
					RSSArticle article = articles.get(position);
					
					Intent read = new Intent(getActivity(), RSSReadActivity.class);
					read.putExtra("title", article.getTitle());
					read.putExtra("description", article.getDescription());
					read.putExtra("img", article.getImg());
					startActivity(read);
				}
			});
			return rootView;
		}
	}
	
	private class AsyncLoadArticles extends AsyncTask<Void, Integer, Boolean> {
		protected void onPreExecute() {
			pDialog.setTitle("L�tfen bekleyiniz...");
			pDialog.setMessage("Bilgiler g�ncelleniyor...");
			pDialog.show();
		}
		@Override
		protected Boolean doInBackground(Void... params) {
			articles = RSSReader.getLatestRssFeed(feedUrl);
			Log.d("RSSListActivity", "Got " + articles.size() + " items.");
			return !articles.isEmpty();
		}
		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if(pDialog.isShowing()) pDialog.dismiss();
			if(noNetworkDialog.isShowing()) noNetworkDialog.dismiss();
			if(!result) {
				Log.d("Main.RefreshList","noNetworkDialog showing");
				noNetworkDialog.show();
			} else {
				rssListAdapter = new RSSListAdapter(RSSListActivity.this, articles, R.layout.item_rss_list, R.id.thumbnail, R.id.title);
				
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						getSupportFragmentManager().beginTransaction()
						.add(R.id.container, new RSSListFragment()).commit();
					}
				});
			}
		}
	}

}

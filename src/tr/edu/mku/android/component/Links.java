package tr.edu.mku.android.component;

/**
 * Contains the URLs for the services.
 * 
 * @author Mehmet Seckin
 *
 */
public class Links {
	public static final String URL_MAIN_SITE = "http://www.mku.edu.tr";
	public static final String URL_MAIL = "https://posta.mku.edu.tr/mail/";
	public static final String URL_ANNOUNCEMENTS_ARCHIVE = "http://mku.edu.tr/mobil/list.php?type=d";
	public static final String URL_EVENTS_ARCHIVE = "http://mku.edu.tr/mobil/list.php?type=e";
	public static final String URL_AKADEMIK_TAKVIM = "http://bapi.mku.edu.tr/android/akademik.php";
	public static final String URL_KATALOG_TARAMA = "http://onlinekatalog.mku.edu.tr/";
	
	public static final String FEED_REHBER = "http://bapi.mku.edu.tr/android/rehber.php";	
	public static final String FEED_NEWS = "http://www.mku.edu.tr/feed.php?lang=tr&type=news";
	public static final String FEED_ANNOUNCEMENTS = "http://www.mku.edu.tr/feed.php?lang=tr&type=ann";
	public static final String FEED_EVENTS = "http://www.mku.edu.tr/feed.php?lang=tr&type=events";
	public static final String FEED_YEMEK_LISTESI = "http://bapi.mku.edu.tr/android/yemek.php";

	
	public static final String SERVICE_OBS = "http://bapi.mku.edu.tr/android/ogrenci/giris.php"; // ��renci bilgi sistemi
	public static final String SERVICE_ABS = "http://www.mku.edu.tr/akademik_giris.php"; // Akademisyen bilgi sistemi
	public static final String SERVICE_EDBS = "http://ekders.mku.edu.tr/ekdersmobile.php"; // Ek ders bilgi sistemi
	
	public static final String STREAM_RADYOUNIVERSITE = "http://streaming.mku.edu.tr:8000/stream.mp3.mp3";

}

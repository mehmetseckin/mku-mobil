package tr.edu.mku.android.component;

/**
 * Stores the user data for the application.
 * 
 * @author Mehmet Seckin
 *
 */
public class User {
	public static final int TYPE_OGRENCI = 0;
	public static final int TYPE_AKADEMISYEN = 1;
	
	public User(int type) {
		this.setType(type);
	}
	
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(int type) {
		this.type = type;
	}

	private String username;
	private String password;
	private int type;
}

package tr.edu.mku.android.phonebook;

import java.util.ArrayList;

import tr.edu.mku.android.R;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

public class PhonebookAdapter extends BaseAdapter implements Filterable {

	private Activity activity;
	private ArrayList<PhonebookRecord> data;
	private ArrayList<PhonebookRecord> fallback;
	private static LayoutInflater inflater = null;
	
	public PhonebookAdapter(Activity a, ArrayList<PhonebookRecord> d) {
		super();
		activity = a;
		data = d;
		fallback = d;
	    inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		if(convertView==null) {
			v = inflater.inflate(R.layout.item_phonebook, null);
		}
		
		PhonebookRecord record = data.get(position);
		String name = record.getPrefix() + " " + record.getName();
		String department = record.getDepartment();
		
		TextView txtName = (TextView) v.findViewById(R.id.txtName);
		TextView txtDept = (TextView) v.findViewById(R.id.txtDept);
		TextView txtNumber = (TextView) v.findViewById(R.id.txtNumber);
		TextView txtExtension = (TextView) v.findViewById(R.id.txtExtension);
		
		txtName.setText(name);
		txtDept.setText(department);
		txtNumber.setText("Tel : " + record.getNumber());
		txtExtension.setText("Dahili : " + record.getExtension());
		return v;
	}

	@Override
	 public Filter getFilter() {

        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
            	if(constraint.length() > 0 )
            		data = (ArrayList<PhonebookRecord>) results.values;
            	else
            		data = fallback;
            	notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                FilterResults results = new FilterResults();
                ArrayList<PhonebookRecord> filteredRecords = new ArrayList<PhonebookRecord>();

                // perform your search here using the searchConstraint String.

                constraint = constraint.toString().toLowerCase();
                for (int i = 0; i < fallback.size(); i++) {
                    String dataNames = fallback.get(i).getName();
                    if (dataNames.toLowerCase().startsWith(constraint.toString()))  {
                        filteredRecords.add(fallback.get(i));
                    }
                }

                results.count = filteredRecords.size();
                results.values = filteredRecords;
                Log.e("VALUES", results.values.toString());

                return results;
            }
        };

        return filter;
    }

}

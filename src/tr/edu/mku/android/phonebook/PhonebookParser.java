package tr.edu.mku.android.phonebook;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


public class PhonebookParser extends DefaultHandler {
	private List<PhonebookRecord> records;
	private PhonebookRecord record;
	private String tempVal;
	
	public PhonebookParser() {
		setRecords(new ArrayList<PhonebookRecord>());
	}

	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		// Reset
		tempVal = "";
		if (qName.equalsIgnoreCase("kisi")) {
			// create a new instance of Meal
			record = new PhonebookRecord();
		}
	}
	
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		tempVal = new String(ch, start, length);
	}

	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		if (qName.equalsIgnoreCase("rehber")) {

		} else if (qName.equalsIgnoreCase("isim")) {
			record.setName(tempVal);
		} else if (qName.equalsIgnoreCase("unvan")) {
			record.setPrefix(tempVal);
		} else if (qName.equalsIgnoreCase("numara")) {
			record.setNumber(tempVal);
		} else if (qName.equalsIgnoreCase("dahili")) {
			record.setExtension(tempVal);
		} else if (qName.equalsIgnoreCase("bolum")) {
			record.setDepartment(tempVal);
			getRecords().add(record);
		}
	}
	
	/**
	 * @return the records
	 */
	public List<PhonebookRecord> getRecords() {
		return records;
	}

	/**
	 * @param records the records to set
	 */
	public void setRecords(List<PhonebookRecord> records) {
		this.records = records;
	}
}

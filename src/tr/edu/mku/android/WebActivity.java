package tr.edu.mku.android;

import org.apache.http.util.EncodingUtils;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WebActivity extends ActionBarActivity {

	static String url;
	static byte[] postData;
	static ProgressDialog dialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_web);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}

		String title = getIntent().getExtras().getString("title");
		url = getIntent().getExtras().getString("url");
		postData = EncodingUtils.getBytes(getIntent().getExtras().getString("postData"), "BASE64");
		dialog = new ProgressDialog(WebActivity.this);
		setTitle(title);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.web, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_web, container,
					false);
			WebView webView = (WebView) rootView.findViewById(R.id.webView);
			//webView.getSettings().setJavaScriptEnabled(true);
			webView.getSettings().setBuiltInZoomControls(true);
			webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
			Log.d("WebActivity", "Loading " + url);
			webView.setWebViewClient(new WebViewClient() {
		        @Override
		        public void onPageFinished(WebView view, String url) {                  
		            if (dialog.isShowing()) {
		                dialog.dismiss();
		            }
		        }
		    });
			dialog.setTitle("L�tfen bekleyiniz");
		    dialog.setMessage("Sayfa y�kleniyor...");
		    dialog.setCanceledOnTouchOutside(false);
		    dialog.show();
		    if(postData.length > 0)
		    	webView.postUrl(url, postData);
		    else
		    	webView.loadUrl(url);
		    return rootView;
		}
	}

}

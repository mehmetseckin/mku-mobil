package tr.edu.mku.android;

import tr.edu.mku.android.rss.RSSArticle;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class RSSReadActivity extends ActionBarActivity {

	static TextView txtTitle, txtDescription;
	static ImageView imgThumb;
	static RSSArticle article;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_rss_read);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
		
		Bundle extras = getIntent().getExtras();
		String title = extras.getString("title");
		String description = extras.getString("description");
		Bitmap img = (Bitmap) getIntent().getParcelableExtra("img");
		this.setTitle(title);
		
		if(description.startsWith("<br />")) {
			description = description.substring(6, description.length());
		}
		
		article = new RSSArticle();
		article.setTitle(title);
		article.setDescription(description);
		article.setImg(img);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.rssread, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_rss_read,
					container, false);
			
			txtTitle = (TextView) rootView.findViewById(R.id.txtTitle);
			txtDescription = (TextView) rootView.findViewById(R.id.txtDescription);
			imgThumb = (ImageView) rootView.findViewById(R.id.imgThumb);
			
			txtTitle.setText(article.getTitle());
			txtDescription.setText(article.getDescription());
			imgThumb.setImageBitmap(article.getImg());
			return rootView;
		}
	}

}
